import { UnicornsBar } from './UnicornsBar';
import * as PIXI from 'pixi.js/dist/pixi.js';
import 'pixi-spine';
import * as FontFaceObserver from 'fontfaceobserver';
// import 'pixi-heaven';
import { TweenLite, Sine } from 'gsap';
import { Star } from './Star';
import { UnicornCard } from './UnicornCard';


export interface IUnicornLoveRoomModel {
  id: number;
  img: string;
  score: number;
  name: string;
  skills: {
    agility: number,
    charisma: number,
    intelligence: number,
    speed: number,
    strength: number
  };
}

export interface ISiringResult {
  id: number;
  score: number;
}

export class LoverRoomView {

  SIRING_IN_PROGRESS = 'Siring in progress';
  SIRIN_COMPLETED = 'Siring completed';

  TOTAL_ROUNDS = 3;

  BASE_URL = './assets/animations/loveroom/';

  app: PIXI.Application;

  rootContainer: PIXI.Container;

  currentRound = 0;

  public sceneCoords: any = {
    x: 0, y: 0, width: 0, height: 0
  };

  spineSceneWidth = 3500;
  spineSceneHeight = 2750;
  spineSceneOffsetY = 0;

  loaded = false;

  background: PIXI.spine.Spine;
  screen: PIXI.spine.Spine;

  screenTopLeft: PIXI.spine.Spine;
  screenTopRight: PIXI.spine.Spine;
  screenCenter: PIXI.spine.Spine;
  screenBottomLeft: PIXI.spine.Spine;
  screenBottomRight: PIXI.spine.Spine;

  screens: PIXI.spine.Spine[] = [];

  shuffleScreensArray: PIXI.spine.Spine[] = [];


  structure: PIXI.spine.Spine;

  moire: PIXI.spine.Spine;

  heart: PIXI.spine.Spine;

  heartSmallBottomLeft: PIXI.spine.Spine;
  heartSmallBottomRight: PIXI.spine.Spine;
  heartSmallTopRight: PIXI.spine.Spine;
  heartSmallTopLeft: PIXI.spine.Spine;
  heartSmallCenter: PIXI.spine.Spine;

  heartsSmall: PIXI.spine.Spine[] = [];

  items: PIXI.spine.Spine[] = [];

  unicornsBar: UnicornsBar;

  unicornsForPreshuffle: number[] = [];

  siringInProgressTextField: PIXI.Text;



  timeToStopShuffle = false;



  shuffleInterval;


  public started = false;
  public siringInProgress = false;
  public shuffling = false;



  mainSkill: string;
  secondarySkill: string;

  skills: string[] = ['agility', 'charisma', 'intelligence', 'speed', 'strength'];

  constructor(private canvas: HTMLCanvasElement,
    private ready: () => void,
    private awaitNextRound: () => void,
    private unicornModels: IUnicornLoveRoomModel[],
    private width: number = 1300,
    private height: number = 600) {
    this.app = new PIXI.Application({
      transparent: true,
      width: this.width,
      height: this.height,
      view: canvas,
      pixelRation: 2
    });
    PIXI.settings.MIPMAP_TEXTURES = true;

    this.rootContainer = new PIXI.Container();
    this.app.stage.addChild(this.rootContainer);
    this.rootContainer.alpha = 0;
    this.mainSkill = this.skills.splice(Math.floor(Math.random() * this.skills.length), 1)[0];
    this.secondarySkill = this.skills.splice(Math.floor(Math.random() * this.skills.length), 1)[0];
    this.loadJsons();
  }



  loadJsons(): void {
    PIXI.loader
      .add('background', `${this.BASE_URL}background.json`)
      .add('screen', `${this.BASE_URL}screen.json`)
      .add('screenTopLeft', `${this.BASE_URL}screen_top_left.json`)
      .add('screenTopRight', `${this.BASE_URL}screen_top_right.json`)
      .add('screenCenter', `${this.BASE_URL}screen_center.json`)
      .add('screenBottomLeft', `${this.BASE_URL}screen_bottom_left.json`)
      .add('screenBottomRight', `${this.BASE_URL}screen_bottom_right.json`)
      .add('structure', `${this.BASE_URL}structure.json`)
      .add('heart', `${this.BASE_URL}heart.json`)
      .add('star', `${this.BASE_URL}star.png`)
      .add('moire', `${this.BASE_URL}moire.json`)
      .add('lover0', `${this.BASE_URL}enemy-lovers/0.png`)
      .add('lover1', `${this.BASE_URL}enemy-lovers/1.png`)
      .add('lover2', `${this.BASE_URL}enemy-lovers/2.png`)
      .add('lover3', `${this.BASE_URL}enemy-lovers/3.png`)
      .add('lover4', `${this.BASE_URL}enemy-lovers/4.png`)
      .add('heartSmallBottomLeft', `${this.BASE_URL}heart_bottom_left.json`)
      .add('heartSmallBottomRight', `${this.BASE_URL}heart_bottom_right.json`)
      .add('heartSmallTopLeft', `${this.BASE_URL}heart_top_left.json`)
      .add('heartSmallTopRight', `${this.BASE_URL}heart_top_right.json`)
      .add('heartSmallCenter', `${this.BASE_URL}heart_center.json`)

      .add(this.mainSkill, `${this.BASE_URL}skills/${this.mainSkill}.png`)
      .add(this.secondarySkill, `${this.BASE_URL}skills/${this.secondarySkill}.png`);

    for (let i = 0; i < this.unicornModels.length; i++) {
      PIXI.loader.add(i.toString(10), this.unicornModels[i].img);
    }


    PIXI.loader.load((loader, resources) => {
      this.background = new PIXI.spine.Spine(resources.background.spineData);
      this.items.push(this.background);
      this.rootContainer.addChild(this.background);
      this.background.state.setAnimation(0, 'animation', true);


      this.screen = new PIXI.spine.Spine(resources.screen.spineData);
      this.items.push(this.screen);
      this.rootContainer.addChild(this.screen);
      this.screen.state.setAnimation(0, 'animation', true);


      const rooms: string[] = [
        'screenBottomLeft',
        'screenBottomRight',
        'screenTopLeft',
        'screenTopRight',
        'screenCenter',
      ];

      for (const room of rooms) {
        this[room] = new PIXI.spine.Spine(resources[room].spineData);
        this.items.push(this[room]);
        this.rootContainer.addChild(this[room]);
        this[room].state.setAnimation(0, 'idle', false);
        this.screens.push(this[room]);


        const hackedTexture: PIXI.Texture = PIXI.Texture.from('lover' + rooms.indexOf(room));
        // console.log('texture:', hackedTexture);
        // tslint:disable-next-line:max-line-length
        this[room].hackTextureBySlotName('1', hackedTexture);


      }

      this.structure = new PIXI.spine.Spine(resources.structure.spineData);
      this.items.push(this.structure);
      this.rootContainer.addChild(this.structure);
      this.structure.state.setAnimation(0, 'idle', false);
      this.structure.state.addListener({
        complete: () => {
          if (this.shuffling) {
            this.structure.state.setAnimation(0, 'appear', false);

          } else {
            this.structure.state.setAnimation(0, 'idle', false);
          }
        }
      });


      this.moire = new PIXI.spine.Spine(resources.moire.spineData);
      this.items.push(this.moire);
      this.rootContainer.addChild(this.moire);
      this.moire.state.setAnimation(0, 'idle', true);

      this.heart = new PIXI.spine.Spine(resources.heart.spineData);

      this.items.push(this.heart);
      this.rootContainer.addChild(this.heart);
      this.heart.state.setAnimation(0, 'idle', true);


      const heartsSmall: string[] = [
        'heartSmallBottomLeft',
        'heartSmallBottomRight',
        'heartSmallTopRight',
        'heartSmallTopLeft',
        'heartSmallCenter'
      ];

      for (const item of heartsSmall) {
        this[item] = new PIXI.spine.Spine(resources[item].spineData);
        this.items.push(this[item]);
        this.rootContainer.addChild(this[item]);
        this.heartsSmall.push(this[item]);
      }



      //  this.heartsSmall.state.setAnimation(0, 'idle', true);

      this.unicornsBar = new UnicornsBar(this.unicornModels, this.mainSkill, this.secondarySkill);
      this.rootContainer.addChild(this.unicornsBar.container);

      this.loaded = true;

      // this.heart.hackTextureBySlotName('0', PIXI.Texture.fromImage('./assets/temp/0.png'));

      for (let i = 0; i < 5; i++) {
        const hackedTexture: PIXI.Texture = PIXI.Texture.from(i.toString(10));
        // console.log('texture:', hackedTexture);
        // tslint:disable-next-line:max-line-length
        this.heart.hackTextureBySlotName(i.toString(10), hackedTexture, hackedTexture.orig);
        // this.heart.hackTextureByName(4, hackedTexture, hackedTexture.orig);
      }


      //




      if (this.background.state.hasAnimation('animation')) {
        this.background.state.setAnimation(0, 'animation', true);
      }

      if (this.screen.state.hasAnimation('animation')) {
        this.screen.state.setAnimation(0, 'animation', true);
      }

      if (this.moire.state.hasAnimation('idle')) {
        this.moire.state.setAnimation(0, 'idle', true);
      }

      /*  setTimeout(() => {
        for (const item of this.unicornModels) {
          item.score = Math.floor(10000 * Math.random());
        }

        this.unicornsBar.updateScore();
      }, 3000);
      */

      const dfDin = new FontFaceObserver('DINPro', { weight: 400 });
      dfDin.load().then(() => {
        // console.log(this.container);
        this.siringInProgressTextField = new PIXI.Text();
        this.siringInProgressTextField.style.fontFamily = 'DINPro';
        this.siringInProgressTextField.style.fill = 0xFBC8F5;
        this.siringInProgressTextField.text = this.SIRING_IN_PROGRESS;
        this.siringInProgressTextField.style.fontSize = 34;
        this.siringInProgressTextField.x = this.width / 2;
        this.siringInProgressTextField.y = this.height * 0.22;
        this.siringInProgressTextField.anchor.x = 0.5;
        this.siringInProgressTextField.anchor.y = 0.5;
        this.siringInProgressTextField.alpha = 0;
        this.siringInProgressTextField.style.align = 'center';
        this.siringInProgressTextField.widht = 1000;
        this.rootContainer.addChild(this.siringInProgressTextField);

        this.app.start();
        this.ready();
        this.resize(this.width, this.height);

        const results: ISiringResult[] = [];
        for (let i = 0; i < 5; i++) {
          results.push({ id: i, score: Math.floor(150 + (1500 - 150) * Math.random()) });
        }

        TweenLite.to(this.rootContainer, 1, { alpha: 1 });
      }, (reject) => {
        console.log('reject');
      });
    });
  }

  convertCoordinates(x: number, y: number): { x: number, y: number } {
    const scale = Math.min(this.width / this.spineSceneWidth, this.height / this.spineSceneHeight);
    return { x: x, y: y };
  }


  public resize(width: number, height: number): void {
    if (this.loaded) {
      this.width = width;
      this.height = height;
      this.app.renderer.resize(width, height);
      const scale = Math.min(this.width / this.spineSceneWidth, this.height / this.spineSceneHeight);
      for (const item of this.items) {
        const sprite: PIXI.heaven.spine.Spine = item;
        sprite.scale.set(scale);
        sprite.position.set(this.width / 2, this.height / 2 + this.spineSceneOffsetY * scale);
      }


      this.siringInProgressTextField.x = this.width / 2;
      this.siringInProgressTextField.y = this.height / 2 - this.spineSceneHeight * (scale / 2) * 0.56;

      this.siringInProgressTextField.scale.set(scale * 5, scale * 5);

      this.unicornsBar.container.width = Math.min(900 * (scale * 4.5), this.width);
      this.unicornsBar.container.scale.y = this.unicornsBar.container.scale.x;
      this.unicornsBar.container.x = this.width / 2 - this.unicornsBar.container.width / 2;
      this.calcSceneCoords();
    }


  }

  calcSceneCoords(): void {
    if (this.loaded) {
      this.sceneCoords.x = this.background.x - this.background.width / 2;
      this.sceneCoords.y = this.background.y - this.background.height / 2;
      this.sceneCoords.width = this.background.width;
      this.sceneCoords.height = this.background.height;
    }
  }




  // game flow

  public start(): void {
    this.started = true;
    let currentUnicornShowed = 0;
    this.shuffling = true;
    this.timeToStopShuffle = false;
    this.unicornsForPreshuffle = [];
    this.heart.state.listeners.length = 0;
    this.heart.state.addListener({
      complete: (entry) => {
        if (currentUnicornShowed < 5) {
          this.unicornsForPreshuffle.push(currentUnicornShowed);
          //   this.shuffleScreens();
          this.heart.state.setAnimation(0, 'appear_' + currentUnicornShowed, false);
          currentUnicornShowed++;

          if (currentUnicornShowed === 5) {
            this.shuffling = false;
            setTimeout(() => {
              clearInterval(this.shuffleInterval);
              this.timeToStopShuffle = true;
            }, 3000);
          }
        } else {
          this.heart.state.setAnimation(0, 'idle', true);

        }
      }
    });

    this.shuffleScreensArray = [];
    this.shuffleScreensArray = this.screens.concat();

    for (const item of this.screens) {
      item.state.listeners.length = 0;
      item.state.addListener({
        complete: (entity) => {
          if (entity.animation) {
            if (entity.animation.name) {
              if (entity.animation.name.indexOf('k') === 0) {
                this.shuffleScreensArray.push(item);

                this.unicornsForPreshuffle.push(parseInt(entity.animation.name.charAt(1), 10) - 1);
                if (this.timeToStopShuffle) {
                  if (this.unicornsForPreshuffle.length === 5) {
                    this.startSiring();
                  }
                }

              }
            }
          }
          //          this.shuffleScreens();
        }
      });
    }

    this.shuffleInterval = setInterval(() => {
      this.shuffleScreens();
    }, 500);

    //  this.heart.state.setAnimation(0, 'appear_0', false);
  }


  public doneSiring(results: ISiringResult[]): void {
    this.siringInProgress = false;

    results.sort((a, b) => {
      return a.score - b.score;
    });


    const rooms: string[] = [
      'screenBottomLeft',
      'screenBottomRight',
      'screenTopLeft',
      'screenTopRight',
      'screenCenter',
    ];

    const heartsSmall: string[] = [
      'heartSmallBottomLeft',
      'heartSmallBottomRight',
      'heartSmallTopLeft',
      'heartSmallTopRight',
      'heartSmallCenter'
    ];


    let currentShow = 0;

    for (let i = 0; i < 5; i++) {
      const unicornOrderNum: number = this.unicornModels.indexOf(this.getUnicornById(results[i].id));
      const hackedTexture: PIXI.Texture = PIXI.Texture.from(unicornOrderNum.toString());
      hackedTexture.smoothing = true;
      this[rooms[i]].hackTextureBySlotName('0', hackedTexture);
      this[rooms[i]].state.listeners.length = 0;
      this[rooms[i]].state.addListener({
        complete: (entry) => {
          if (currentShow >= i) {
            if (entry.animation.name === 'await_idle') {
              setTimeout(() => {
                this[heartsSmall[i]].state.setAnimation(0, 'off', false);
              }, 800);
              this[rooms[i]].state.setAnimation(0, 'await_off', false);
            } else if (entry.animation.name === 'await_off') {
              this[rooms[i]].state.setAnimation(0, 'open', false);
              this.createStar(results[i], i);
            } else if (entry.animation.name === 'open') {
              this[rooms[i]].state.setAnimation(0, 'open_idle', true);

            }
          }
        }
      });
      // this[rooms[i]].state.setAnimation(0, 'open', false);
    }

    setInterval(() => {
      currentShow++;
    }, 2500);

  }

  createStar(item: ISiringResult, roomId: number): void {
    const star: Star = new Star(item.score);


    const scale = Math.min(this.width / this.spineSceneWidth, this.height / this.spineSceneHeight);


    if (roomId === 0) {
      star.container.x = this.width / 2 - this.spineSceneWidth * (scale / 2) * 0.62;
      star.container.y = this.height / 2 + this.spineSceneHeight * (scale / 2) * 0.1;

    } else if (roomId === 1) {
      star.container.x = this.width / 2 + this.spineSceneWidth * (scale / 2) * 0.68;
      star.container.y = this.height / 2 + this.spineSceneHeight * (scale / 2) * 0.1;

    } else if (roomId === 2) {
      star.container.x = this.width / 2 - this.spineSceneWidth * (scale / 2) * 0.68;
      star.container.y = this.height / 2 - this.spineSceneHeight * (scale / 2) * 0.45;

    } else if (roomId === 3) {
      star.container.x = this.width / 2 + this.spineSceneWidth * (scale / 2) * 0.68;
      star.container.y = this.height / 2 - this.spineSceneHeight * (scale / 2) * 0.45;

    } else if (roomId === 4) {
      star.container.x = this.width / 2; // + this.spineSceneWidth * (scale / 2) * 0.68;
      star.container.y = this.height / 2 - this.spineSceneHeight * (scale / 2) * 0.45;

    }





    const card: UnicornCard = this.unicornsBar.getCardById(item.id);



    setTimeout(() => {
      this.rootContainer.addChild(star.container);

      star.show();

    }, 50);



    setTimeout(() => {
      star.startRotation();
    }, 500);

    setTimeout(() => {
      TweenLite.to(star.container, 0.8, {
        y: (this.unicornsBar.container.height * this.unicornsBar.container.scale.y) / 3,
        ease: Sine.easeIn
      });
      TweenLite.to(star.container, 0.8, {
        x: card.container.x * this.unicornsBar.container.scale.x + this.unicornsBar.container.x + card.container.width / 2 * this.unicornsBar.container.scale.x,
        ease: Sine.easeOut
      });



      TweenLite.to(star.container, 0.75, {
        onComplete: () => {

          this.getUnicornById(item.id).score += item.score;
          this.unicornsBar.updateScore();

          star.hide();
          if (roomId === 4) {
            this.roundEnd();
          }
        }
      });
    }, 1500);

  }

  roundEnd(): void {
    this.currentRound++;

    if (this.currentRound < this.TOTAL_ROUNDS) {
      setTimeout(() => {
        for (const item of this.screens) {
          item.state.listeners.length = 0;
          item.state.setAnimation(0, 'close', false);
        }
        this.heart.state.listeners.length = 0;

        TweenLite.to(this.siringInProgressTextField, 1, {
          alpha: 0, onComplete: () => {
            this.siringInProgressTextField.text = this.SIRING_IN_PROGRESS;
          }
        });

        setTimeout(() => {
          this.start();
        }, 500);

      }, 100);
    } else {
      console.log('done!');
    }

  }

  getUnicornById(id: number): IUnicornLoveRoomModel {
    for (const item of this.unicornModels) {
      if (item.id === id) {
        return item;
      }
    }

    return null;
  }

  shuffleScreens(): void {
    if (this.unicornsForPreshuffle.length !== 0) {

      const num: number = this.unicornsForPreshuffle.splice(Math.floor(this.unicornsForPreshuffle.length * Math.random()), 1)[0] + 1;
      const screen: PIXI.spine.Spine = this.shuffleScreensArray.splice(Math.floor(this.shuffleScreensArray.length * Math.random()), 1)[0];
      screen.state.setAnimation(0, 'k' + num, false);

    }
  }


  startSiring(): void {
    this.siringInProgress = true;
    this.awaitNextRound();
    this.textFieldBlink();

    for (const item of this.screens) {
      setTimeout(() => {
        item.state.setAnimation(0, 'await', false);
        item.state.listeners.length = 0;
        item.state.addListener({
          complete: () => {
            if (this.siringInProgress) {
              item.state.setAnimation(0, 'await_idle', true);
            }
          }
        });

      }, Math.random() * 50);

    }

    for (const item of this.heartsSmall) {
      setTimeout(() => {
        item.state.setAnimation(0, 'on', false);
      }, Math.random() * 50);
    }
  }



  textFieldBlink(): void {

    if (this.siringInProgress || this.siringInProgressTextField.alpha === 1) {
      TweenLite.to(this.siringInProgressTextField, 1, {
        alpha: this.siringInProgressTextField.alpha === 0 ? 1 : 0, onComplete: () => {
          this.textFieldBlink();
        }
      });
    } else {
      this.siringInProgressTextField.text = this.SIRIN_COMPLETED;
      TweenLite.to(this.siringInProgressTextField, 1, {
        alpha: 1
      });
    }
  }


  public destroy(): void {

    // tslint:disable-next-line:forin
    for (const texture in PIXI.utils.TextureCache) {
      if (PIXI.utils.TextureCache[texture]) {
        const tmp = PIXI.Texture.removeFromCache(texture);
        tmp.destroy(true);
      }
    }
    // tslint:disable-next-line:forin
    for (const texture in PIXI.utils.BaseTextureCache) {
      if (PIXI.utils.BaseTextureCache[texture]) {
        PIXI.utils.BaseTextureCache[texture].destroy(true);
      }
    }
    this.app.stage.destroy({ children: true, baseTexture: true, texture: true });
    this.app.destroy();
  }

}
