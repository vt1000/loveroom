import { TweenLite, Back } from 'gsap';
export class Star {
  container: PIXI.Container;
  scoreNum: PIXI.Text;


  interval;

  constructor(score: number = 1500) {

    this.container = new PIXI.Container();

    this.container.alpha = 0;

    const sprite: PIXI.Sprite = new PIXI.Sprite(PIXI.Texture.from('star'));
    sprite.anchor.set(0.5, 0.5);
    sprite.width = 160;
    sprite.scale.y = sprite.scale.x;
    this.container.addChild(sprite);
    //  sprite.cacheAsBitmap = true;

    this.scoreNum = new PIXI.Text();
    // this.scoreNum.width = 500;
    this.scoreNum.style.fill = 0xB92636;
    this.scoreNum.text = score.toString(10);
    this.scoreNum.style.fontSize = 34;
    this.scoreNum.style.fontFamily = 'PF Centro Sans Pro UBlack';
    this.scoreNum.x = 0;
    this.scoreNum.rotation = 0.27;
    this.scoreNum.anchor.set(0.5, 0.5);
    this.scoreNum.y = 0;
    this.container.addChild(this.scoreNum);

    this.container.scale.set(0.5, 0.5);
    this.container.rotation = Math.PI * 40 / 360;
  }

  show(): void {
    TweenLite.to(this, 1 / 3, { alpha: 1 });
    TweenLite.to(this, 1 / 2, { scale: 1, ease: Back.easeOut.config(3) });
    TweenLite.to(this, 1 / 2, { rotation: 0 });
  }


  hide(): void {
    TweenLite.to(this, 1 / 3, { alpha: 0 });
    TweenLite.to(this, 1 / 3, {
      scale: 0.5, onComplete: () => {
        this.container.destroy();
        clearInterval(this.interval);
      }
    });
    //  TweenLite.to(this, 15 / 30, { rotation: Math.PI * 40 / 360 });
  }


  public startRotation(): void {
    let k = -0.011;
    this.interval = setInterval(() => {
      this.rotation += k;
      k *= 1.05;
      k = Math.max(-0.1, k);
    }, 1000 / 60);
  }

  set rotation(value: number) {
    this.container.rotation = value;
  }

  get rotation(): number {
    return this.container.rotation;
  }

  get alpha() {
    return this.container.alpha;
  }
  set alpha(value: number) {
    this.container.alpha = value;
  }

  get scale() {
    return this.container.scale.x;
  }

  set scale(scale: number) {
    this.container.scale.x = scale;
    this.container.scale.y = scale;
  }

}
