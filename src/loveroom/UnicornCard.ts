import { IUnicornLoveRoomModel } from './LoveRoomView';
import * as FontFaceObserver from 'fontfaceobserver';
// declare var FontFaceObserver;
export class UnicornCard {
  container: PIXI.Container;

  image: PIXI.Sprite;

  mainSkill: PIXI.Sprite;
  mainSkillText: PIXI.Text;
  secondarySkill: PIXI.Sprite;
  secondarySkillText: PIXI.Text;

  nickname: PIXI.Text;

  score: PIXI.Text;


  constructor(public model: IUnicornLoveRoomModel, mainSkill: string, secondarySkill: string) {
    this.container = new PIXI.Container();
    this.image = new PIXI.Sprite(PIXI.Texture.fromImage(model.img));
    const mask: PIXI.Graphics = new PIXI.Graphics();
    mask.beginFill(0, 0.3);
    mask.drawRect(0, 0, 150, 102);
    mask.endFill();
    this.container.addChild(mask);
    this.image.anchor.x = 0.5;
    this.image.scale.x = -0.35;
    this.image.scale.y = 0.35;

    this.image.x = -10;
    this.image.y = -40;
    this.image.mask = mask;
    this.container.addChild(this.image);




    const pfCentroFont = new FontFaceObserver('PF Centro Sans Pro UBlack', {
      weight: 400
    });

    pfCentroFont.load().then(() => {
      this.score = new PIXI.Text();
      this.score.style.fontFamily = 'PF Centro Sans Pro UBlack';
      this.score.style.fontSize = 34;
      // this.score.style.letterSpacing = -0.5;
      this.score.resolution = 2;
      this.score.x = 190;
      // this.score.c
      this.score.style.fill = 0xf3e619;
      this.score.y = 57;
      this.score.anchor.x = 1;
      this.container.addChild(this.score);
      this.updateScore();
    }, (reject) => {
      console.log('reject', reject);
    });

    const dfDin = new FontFaceObserver('DINPro', { weight: 400 });
    dfDin.load().then(() => {
      // console.log(this.container);
      this.nickname = new PIXI.Text();
      this.nickname.style.fontFamily = 'DINPro';
      this.nickname.style.fill = 0xFFFFFF;
      this.nickname.text = model.name;
      this.nickname.style.fontSize = 18;
      this.nickname.x = 190;
      this.nickname.y = 35;
      this.nickname.anchor.x = 1;

      const maxWidth = 100;
      let i = 0;

      while (this.nickname.width > maxWidth || i > 100) {
        this.nickname.text = model.name.slice(0, model.name.length - i) + '...';
        //  this.nickname.updateTransform();
        i++;
        // console.log(this.nickname.text);
      }
      this.nickname.resolution = 4;

      //  console.log(this.nickname);
      this.container.addChild(this.nickname);



      this.secondarySkillText = new PIXI.Text(model.skills[secondarySkill].toString());

      this.container.addChild(this.secondarySkillText);
      this.secondarySkillText.style.fill = 0xFFFFFF;
      this.secondarySkillText.style.fontFamily = 'DINPro';
      this.secondarySkillText.anchor.x = 1;
      this.secondarySkillText.style.fontSize = 15;
      this.secondarySkillText.x = 187;
      this.secondarySkillText.y = 17;


      this.secondarySkill = new PIXI.Sprite(PIXI.Texture.from(secondarySkill));
      this.container.addChild(this.secondarySkill);
      this.secondarySkill.height = 10;
      this.secondarySkill.scale.x = this.secondarySkill.scale.y;
      this.secondarySkill.x = this.secondarySkillText.x - this.secondarySkillText.width - 4 - this.secondarySkill.width;
      this.secondarySkill.y = this.secondarySkillText.y + 4;

      this.mainSkillText = new PIXI.Text(model.skills[mainSkill].toString());
      this.mainSkillText.style.fill = 0xf3e619;
      this.mainSkillText.anchor.x = 1;
      this.mainSkillText.style.fontFamily = 'DINPro';
      this.mainSkillText.style.fontSize = 25;
      this.mainSkillText.x = Math.round(this.secondarySkill.x - 13);
      this.mainSkillText.y = 8;
      this.mainSkillText.resolution = 4;
      this.container.addChild(this.mainSkillText);

      this.mainSkill = new PIXI.Sprite(PIXI.Texture.from(mainSkill));
      this.mainSkill.height = 16;
      this.mainSkill.scale.x = this.mainSkill.scale.y;
      this.mainSkill.x = this.mainSkillText.x - this.mainSkillText.width - 3 - this.mainSkill.width;
      this.mainSkill.y = this.mainSkillText.y + 7;
      this.container.addChild(this.mainSkill);

    }, (reject) => {
      console.log('reject', reject);
    });




  }

  updateScore(): void {
    //  console.log('update score', this.model);
    this.score.text = this.model.score.toString();
  }
}
