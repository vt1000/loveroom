import { TweenLite } from 'gsap';
import { UnicornCard } from './UnicornCard';

export class UnicornsBar {
  public container: PIXI.Container;
  public cards: UnicornCard[];
  constructor(unicorns: any[], mainSkill: string, secondarySkill: string) {
    this.container = new PIXI.Container();

    const bg: PIXI.Graphics = new PIXI.Graphics();
    bg.beginFill(0x1F0621, 0.75);
    bg.drawRect(0, 0, 1000, 102);
    bg.endFill();
    this.container.addChild(bg);
    // temp.alpha = 0;


    const canvas = document.createElement('canvas');
    canvas.width = 1000;
    const ctx = canvas.getContext('2d');

    const gradient = ctx.createLinearGradient(0, 0, 994, 35);
    gradient.addColorStop(0, '#00F4FF17');
    gradient.addColorStop(1, '#00FAFF6E');
    ctx.fillStyle = gradient;
    ctx.fillRect(0, 0, 994, 35);

    const gradientSprite: PIXI.Sprite = new PIXI.Sprite(PIXI.Texture.fromCanvas(canvas));
    gradientSprite.y = 244 / 4;
    this.container.addChild(gradientSprite);

    this.cards = [];
    for (let i = 0; i < unicorns.length; i++) {
      const unicornCard = new UnicornCard(unicorns[i], mainSkill, secondarySkill);
      //  const unicornCard = new UnicornCard(unicorns[0], mainSkill, secondarySkill);
      this.container.addChild(unicornCard.container);
      unicornCard.container.x = 1000 / unicorns.length * i;
      this.cards.push(unicornCard);
    }



  }

  getCardById(id: number): UnicornCard {
    for (const card of this.cards) {
      if (card.model.id === id) {
        return card;
      }
    }
    return null;
  }

  updateScore(): void {
    for (const item of this.cards) {
      item.updateScore();
    }
    this.cards.sort((a, b) => {
      return a.model.score - b.model.score;
    });

    for (let i = 0; i < this.cards.length; i++) {
      TweenLite.to(this.cards[i].container, 1, { x: 1000 / this.cards.length * i });
    }

  }
}
