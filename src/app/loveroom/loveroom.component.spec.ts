import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoveroomComponent } from './loveroom.component';

describe('LoveroomComponent', () => {
  let component: LoveroomComponent;
  let fixture: ComponentFixture<LoveroomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoveroomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoveroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
