import { ISiringResult } from './../../loveroom/LoveRoomView';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LoverRoomView } from '../../loveroom/LoveRoomView';

@Component({
  selector: 'app-loveroom',
  templateUrl: './loveroom.component.html',
  styleUrls: ['./loveroom.component.css']
})
export class LoveroomComponent implements OnInit {


  roomAnimation: LoverRoomView;

  ready = false;

  @ViewChild('canvas') canvas: ElementRef;

  constructor() { }

  ngOnInit() {
    this.roomAnimation = new LoverRoomView(
      this.canvas.nativeElement,
      () => {
        this.ready = true;
      },
      () => {
        console.log('awaiting mining');
      },
      [
        {
          id: 0,
          img: './assets/temp/0.png',
          score: 0,
          name: 'Princess',
          skills: {
            agility: 3,
            charisma: 6,
            intelligence: 7,
            speed: 4,
            strength: 6
          }
        },
        {
          id: 1,
          img: './assets/temp/1.png',
          score: 0,
          name: 'Unicorn #32123',
          skills: {
            agility: 2,
            charisma: 3,
            intelligence: 4,
            speed: 5,
            strength: 7
          }
        },
        {
          id: 2,
          img: './assets/temp/2.png',
          score: 0,
          name: 'Ololo',
          skills: {
            agility: 9,
            charisma: 7,
            intelligence: 6,
            speed: 5,
            strength: 4
          }
        }, {
          id: 3,
          img: './assets/temp/3.png',
          score: 0,
          name: 'Happy',
          skills: {
            agility: 2,
            charisma: 1,
            intelligence: 2,
            speed: 3,
            strength: 6
          }
        }, {
          id: 4,
          img: './assets/temp/4.png',
          score: 0,
          name: 'Queen',
          skills: {
            agility: 8,
            charisma: 9,
            intelligence: 7,
            speed: 6,
            strength: 4
          }
        },
      ],
      window.innerWidth,
      window.innerHeight);

    window.addEventListener('resize', () => {
      this.roomAnimation.resize(window.innerWidth, window.innerHeight);
    });
  }


  doneSiring(): void {
    const results: ISiringResult[] = [];
    for (let i = 0; i < 5; i++) {
      results.push({ id: i, score: Math.floor(150 + (1500 - 150) * Math.random()) });
    }

    this.roomAnimation.doneSiring(results);
  }

}
